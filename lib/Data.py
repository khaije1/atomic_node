#/usr/bin/env python
"""
 DataGrep.py - module in CertManager_Proponet.py suite

"""
#-project information
_project_information = {
	"name"			:"",
	"www"			:"",
	"license"		:"",
	"maturity"		:"1 - pre-alpha",
	"purpose"		:""
}
#-local metainfo
_local_metainfo = {
	"name"			:"grokData",
	"email"			:"",
	"license"		:"",
	"author"		:"nick garber",
	"purpose"		:"",
	"state"			:"unstable",
	"ver.major"		:"0",
	"ver.minor"		:"1",
	"ver.revision"	:"a"
}
#-static variables
#-global variables
#-modules
import os, sys, fnmatch, re, time
import grokHive, AtomicData
from datetime import date, timedelta, datetime
#-class defs
#-function defs


class grokData():
	"""
	hive keys 
	 referenced/required:
		Scenario Attributes-
		strings basepath , relapath , branchpath , filename
		filematchregex{}
		int expiryTime

		Derived Attributes-
		fileAttr:: modDateTime

		Deduced Attributes-
		perfile attributes::  , matchOnRegex[filematchregex]
	 modified/appended:
		Scenario Attributes-
		Derived Attributes-
		Deduced Attributes-
	"""

	def __init__(self, anchor):
		"""
		the anchor will probably be the record of an individual file.
		"""
		self.anchor = anchor
		pass
	def getConfig():
		"""
		nts: i have a few options here... (1)derive the config hive by reverse-traversal from anchor, that may require doubly-linked nodes which aren't presently implimented, or (2)have it passed in with each file, or (3)have it query the HiveGrok, or possibly even a new ConfigGrok, directly for the needed info
		"""
	def setKeyPaths():
		#config keys
		self.keyConfig = 'configuration'
		self.keyFileMatchRegex = 'fileMatchRegex'
		self.keyExpiryTime = 2 #in days
		#file keys
		self.keyBasePath = 'basepath'
		self.keyRelaPath = 'relapath'
		self.keyBranchPath = 'branchpath'
		self.keyFileName = 'filename'
		self.keyFileModTime = 'fileModTime'
		self.keyFileRegexMatches = 'fileRegexMatches'
		#data keys
#		self.keyFileNumOfBytes = ''
#		self.keyFileNumOfRows = ''
#		self.keyFileDataFormat = ''
#		self.keyFileNumOfDataRecords = ''


	def addConfig(newVal):
		#validate newVal
		key = keyConfig
		#create key, if doesn't exist
		if not self.index[key].exists():
			self.index.addkey(key)
		#TODO: add each individual element, if newVal is a list 
		self.index.addval(key, newVal)
		logging.info(key, ": has added", newVal)

	def addBasePath(newVal):
		#validate basepathToAdd
		key = keyBasePath
		pass
		
	def addRelaPath():
		pass
	def addBranchPath():
		pass
	def addFileName():
		pass
	def addFileMatchRegex():
		pass
	def setExpiryTime():
		pass



#
#actual work
#
	targets = []
	#TODO: pocket some of this work into functions
	for relapath in relapaths: 
		print "\n", "Client:", os.path.abspath(basepath + relapath)
		for root,dir,files in os.walk(basepath + relapath):
			targetPerPath=0
			print " -->", os.path.abspath(root)
			for file in files:
				if re.search(filematchregex, file, re.I):
					targetPerPath = targetPerPath + 1
					print os.path.abspath(os.path.join(root, file))
					targets.append(os.path.abspath(os.path.join(root, file)))
			print str(len(files)) , "files traversed."
			print targetPerPath, "targets per path."
			print str(len(targets)) , "targets located so far.", "\n"

	targets.sort()

	d = datetime.now() - timedelta(days=DaysToExpire)
	d = d.timetuple()
	oldfiles = []
	newfiles = []
	for file in targets:
		filepath = os.path.split(file)
		os.chdir(os.path.realpath(filepath[0]))
		filetimesecs = os.path.getctime(filepath[1]) 
		#todo: add sub filestimesecs = getctime -gt getmtime
		filetime = time.localtime(filetimesecs)
		if filetime < d:
			oldfiles.append(file)
			print "*******************************"
			print "path: ", file
			print "modified:", (int(time.mktime(time.strptime(str(datetime.now()), '%Y-%m-%d %H:%M:%S'))) - filetimesecs)/60/60/24 , "days ago."
			#print "*******************************"
		if filetime > d:
			newfiles.append(file)
		os.chdir(basepath)

	print "Old path: %s" % str(len(oldfiles))
	print "New path: %s" % str(len(newfiles))


#TODO: add file write-out options


#-main--begin---------
def main():
	#do nothing
	pass
#-main--end-----------

if __name__ == '__main__':
	main()
