#/usr/bin/env python
"""HiveGrok.py - module in CertManager_Proponet.py suite

"""
#-project information
_project_information = {
	"name"			:"",
	"www"			:"",
	"license"		:"",
	"maturity"		:"1 - pre-alpha",
	"purpose"		:""
}
#-local metainfo
_local_metainfo = {
	"name"			:"HiveGrok",
	"email"			:"",
	"license"		:"",
	"author"		:"",
	"purpose"		:"",
	"state"			:"unstable",
	"ver.major"		:"0",
	"ver.minor"		:"1",
	"ver.revision"	:"a"
}
#-static variables
DEFAULT_BASEPATH = "var/input/"
DEFAULT_RELAPATH = [ 
	"error/",
	"test/"
	]
DEFAULT_FILEMATCHREGEX = ".\.input$"

#-global variables
#-modules
import os, sys, fnmatch, re, time
import AtomicData

#-class defs
class HiveGrok():
	"""
	the main responsibiliy of this class is to manage the 'database-like' access to the hive
	 this includes a certain degree of schema validation

	Scenario Attributes-
	strings basepath , relapath , branchpath , filename
	dict filematchregex{}
	int expiryTime

	Derived Attributes-
	fileAttr:: modDateTime

	Deduced Attributes-
	perfile attributes::  , matchOnRegex[filematchregex]
	"""
	def __init__(self):
        	"""
                if no config provided, use defaults
                """
		self.index = AtomicData.nodes()
		setKeyPaths()
		pass
	def setKeyPaths():
                """
	
                """
		#config keys
		self.keyConfig = 'configuration'
		self.keyFileMatchRegex = 'fileMatchRegex'
		self.keyExpiryTime = 2 #in days
		#file keys
		self.keyBasePath = 'basepath'
		self.keyRelaPath = 'relapath'
		self.keyBranchPath = 'branchpath'
		self.keyFileName = 'filename'
		self.keyFileModTime = 'fileModTime'
		self.keyFileRegexMatches = 'fileRegexMatches'
		#data keys
#		self.keyFileNumOfBytes = ''
#		self.keyFileNumOfRows = ''
#		self.keyFileDataFormat = ''
#		self.keyFileNumOfDataRecords = ''

	def addValue(self,root,key,value,description=""):
		"""
		i need to identify the method to traverse the hive and the minimum complete set of options needed for proper traversal and manipulation
		"""
		#validate value
		#error if key doesn't exist
		if not self.index[key].exists():
			#TODO: raise errMissingKey , msg(key, "key not found"
			pass 
		else: 
			if value.islist():
				logging.warning("attempting to add list of values", newVal,"to key:",key," Each list items will be added individually. This operation has not been validated and may produce undesirable results in some cases."
				for val in newVal:
					self.index.addval(key, val)
					logging.info(key, ": has added", val)

	def addBasePath(newVal):
		#validate basepathToAdd
		key = keyBasePath
		pass
		
	def getPathValid(self, path):
		"""
		if path is valid, return true
		 else false
		"""
		validity = True
		#?: is a validator useful here, or should path be assumed valid unless found otherwise. i suspect validators are unneeded here
		# validators 
		# if path is wellformed and congruent then validity = true

		# invalidators
		# if path matches any excludePath regex's then validity = false
		for regexExcludePath in regexExcludePaths:
			#TODO: if path matches regexExcludePath, set validity = false
			#TODO: add logging for when validity=false
			pass
		return validity
	def getFilesInPath(self, path):
		#TODO: move function vars into class vars where appropriate
		#TODO: add scan information to local Hive, with the option to
		targets = []
		#TODO: pocket some of this work into functions
		for relapath in relapaths: 
			#TODO: add log or assert in place of all the 'print' lines below
			print "\n", "Client:", os.path.abspath(basepath + relapath)
			for root,dir,files in os.walk(basepath + relapath):
				targetPerPath=0
				print " -->", os.path.abspath(root)
				for file in files:
					if re.search(filematchregex, file, re.I):
						targetPerPath = targetPerPath + 1
						print os.path.abspath(os.path.join(root, file))
						targets.append(os.path.abspath(os.path.join(root, file)))
				print str(len(files)) , "files traversed."
				print targetPerPath, "targets per path."
				print str(len(targets)) , "targets located so far.", "\n"
 	def addHive(path=None):
		"""add hive to index
		"""
		pass
 	def remHive(hive):
		"""remove hive from index
		"""
		pass
 	def getHive():
		"""return list of hives
		"""
		pass
 	def getHive(hiveName):
		"""return hive object from hiveName
		"""
		pass
		

#-function defs
#-main--begin---------
def main():
	#do nothing
	pass
#-main--end-----------

if __name__ == '__main__':
	main()
