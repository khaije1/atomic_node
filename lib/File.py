#/usr/bin/env python
"""grokFile.py - description
	
"""
#-project information
_project_information = {
	"name"			:"",
	"www"			:"",
	"license"		:"",
	"maturity"		:"1 - pre-alpha",
	"purpose"		:""
}
#-local metainfo
_local_metainfo = {
	"name"			:"grokFile",
	"email"			:"",
	"license"		:"",
	"author"		:"",
	"purpose"		:"",
	"state"			:"unstable",
	"ver.major"		:"0",
	"ver.minor"		:"1",
	"ver.revision"	:"a"
}
#-static variables
#-global variables
#-modules
import os, sys, fnmatch, re, time
import grokHive
from datetime import date, timedelta, datetime
#-class defs

class grokFile():
	"""
	input
	"""
	def __init__(self, root):
		"""
		if no config provided, use defaults
		"""
		self.index = adata.nodes()
		setKeyPaths()
		pass

	def setKeyPaths():
		#config keys
		self.keyConfig = 'configuration'
		self.keyFileMatchRegex = 'fileMatchRegex'
		self.keyExpiryTime = 2 #in days
		#file keys
		self.keyBasePath = 'basepath'
		self.keyRelaPath = 'relapath'
		self.keyBranchPath = 'branchpath'
		self.keyFileName = 'filename'
		self.keyFileModTime = 'fileModTime'
		self.keyFileRegexMatches = 'fileRegexMatches'
		#data keys
#		self.keyFileNumOfBytes = ''
#		self.keyFileNumOfRows = ''
#		self.keyFileDataFormat = ''
#		self.keyFileNumOfDataRecords = ''


	def addConfig(newVal):
		#validate newVal
		key = keyConfig
		#create key, if doesn't exist
		if not self.index[key].exists():
			self.index.addkey(key)
		#TODO: add each individual element, if newVal is a list 
		self.index.addval(key, newVal)
		logging.info(key, ": has added", newVal)

	def addBasePath(newVal):
		#validate basepathToAdd
		key = keyBasePath
		pass
		
	def addRelaPath():
		pass
	def addBranchPath():
		pass
	def addFileName():
		pass
	def getFileMatchRegex():
		pass
	def getExpiryTime():
		pass

	def getPathValidity(self, path):
		"""
		if path is valid, return true
		 else false
		"""
		validity = True
		#?: is a validator useful here, or should path be assumed valid unless found otherwise. i suspect validators are unneeded here
		# validators 
		# if path is wellformed and congruent then validity = true

		# invalidators
		# if path matches any excludePath regex's then validity = false
		for regexExcludePath in regexExcludePaths:
			#TODO: if path matches regexExcludePath, set validity = false
			#TODO: add logging for when validity=false
			pass
		return validity
	def getFilesInPath(self, path):
		#TODO: move function vars into class vars where appropriate
		#TODO: add scan information to local Hive, with the option to
		targets = []
		#TODO: pocket some of this work into functions
		for relapath in relapaths: 
			#TODO: add log or assert in place of all the 'print' lines below
			print "\n", "Client:", os.path.abspath(basepath + relapath)
			for root,dir,files in os.walk(basepath + relapath):
				targetPerPath=0
				print " -->", os.path.abspath(root)
				for file in files:
					if re.search(filematchregex, file, re.I):
						targetPerPath = targetPerPath + 1
						print os.path.abspath(os.path.join(root, file))
						targets.append(os.path.abspath(os.path.join(root, file)))
				print str(len(files)) , "files traversed."
				print targetPerPath, "targets per path."
				print str(len(targets)) , "targets located so far.", "\n"

	def getTargetMetadata(self,file):
		d = datetime.now() - timedelta(days=DaysToExpire)
		d = d.timetuple()
		oldfiles = []
		newfiles = []
		for file in targets:
			filepath = os.path.split(file)
			os.chdir(os.path.realpath(filepath[0]))
			filetimesecs = os.path.getctime(filepath[1]) 
			#todo: add sub filestimesecs = getctime -gt getmtime
			filetime = time.localtime(filetimesecs)
			if filetime < d:
				#add files to hive
				oldfiles.append(file)
				print "*******************************"
				print "path: ", file
				print "modified:", (int(time.mktime(time.strptime(str(datetime.now()), '%Y-%m-%d %H:%M:%S'))) - filetimesecs)/60/60/24 , "days ago."
				#print "*******************************"
			if filetime > d:
				newfiles.append(file)
		os.chdir(basepath)

		print "Old path: %s" % str(len(oldfiles))
		print "New path: %s" % str(len(newfiles))


#-function defs
#-main--begin---------
def main():
	#do nothing
	pass
#-main--end-----------

if __name__ == '__main__':
	main()
