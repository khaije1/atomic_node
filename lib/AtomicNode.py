#!/usr/bin/env python

#-project information
_project_information = {
	"name"			: "atomic data",
	"www"			: "",
	"license"		: "lgpl",
	"maturity"		: "1 - pre-alpha",
	"purpose"		: "construct abstract, computable, ontologically-sound information models"
}
#-local metainfo
_local_metainfo = {
	"name"			: "",
	"email"			: "",
	"license"		: "",
	"author"		: "",
	"purpose"		: "",
	"state"			: "unstable",
	"ver.major"		: "0",
	"ver.minor"		: "1",
        "ver.revision"          : "a"
}
#-static variables
#-global variables
#-modules
import sys, logging

#-class defs
class tmpMiscMerge():
    """ignore/delete me
    this class only exists to temporarily hold homeless methods
    """
    #TODO: develop/standardize terminology to differentiate between 'node'-keys and regular, or 'inert' dictkeys
    #TODO: develop a means to safely convert from one key-type to another... this would involve moving the key-val pair from the 
    #NB: each node homes 1 regular old dictionary, the default key

    def getval(self, key):
        """returns the key value
        key := the name on the key to check
        if the key exists ==> return key/val pairs from Node.__call__ at self.index[key]
	if key doesn't exist ==> return False

        #NB: this should be normalized to emulate the dictionary-class API as closely as sensible possible
        if key in self.index: return self.index[key]
        else: print "err: unknown key" ; return False
        """
        pass

    def addval(self, key, value): 
        """add a value to a key
        key := the name of the key to add the value to
        value := the value to add to the list of values for this key
        if 'key' exists and 'value' is unique to it, add 'value' ==> True
        if 'key' doesn't exist, or type=node(), or 'value' already exist ==> False
        
        #NB: Initially it was thought that an intrinsic one:many // key:val method was needed.
             With clarity derived from observing the defaultdict() mechanism it becomes clear that with
             forethought this is not necessary. Thankfully this provides knock-in simplicity.
        
        #?: would this be a good place for a case statement
        if key in self.index:
            if isinstance(value, Node ):
                #TODO: add hook, add node
                self.index[key]['value'] = value
                pass
            if value == self.index[key]['value']:
                #TODO: add hook, attempted to set key-val pair which already exists
                print "info: key-pair already exists"
                pass
            else:
                #TODO: add hook, val change 
                self.index[key]['value'] = value
                return True
        else:
            #TODO: add hook, key not found
            print "err: key not in index"
        """
        pass

    def remval(self, key, value):
        """remove a value from a key
        key := the name of the key to check for values
        value := the specific value to remove from the list
        if 'key' exists and 'value' exists in 'key', 'key' is removed ==> True
        if 'key' doesn't exist or 'value' doesn't exist ==> False
        """
        pass

    def findkey(self,searchtoken):
        """returns matching keys and descriptions on this node
        #original function
        if searchtoken in self.index:
            return True
        else:
            return False
        """
        pass

    def findval(self, searchtoken):
        """returns keys with this value on this node
        """
        pass


    def __init__(self, initdict=None):
        """class constructor
        name := the value to apply to the '__class_instance_name' key, defaults to a None object
        
        #original function below
        keyset = {
            'default' : '',
            'label' : '__class_instance_name',
            'desc' : 'the given name of this class instance',
            'progenitor' : '__..',
            'object_id':'__id',
            'object_type':'__type',
            'siblings':'__./.'
            }
        """
        pass
    
class Node():
    """Generic node abstraction.
	This class wraps a dictionary to manage keyNodes used in
	describing the behaviour of the node instance. A default set of
	attributes can be loaded and subsequently manipulated.
	I'm hoping this can provide a suitable container to abstract
	the generic linking of arbitrary Node
        SERVICE NODES: i should create srvcnodes that can be shared rather than instantiating every service on every node

    """
    #TODO: this class needs many more hooks!
    #NB: from Node()'s perspective, every key is a 'Node()'
    #NB: (ref) http://docs.python.org/library/stdtypes.html#dict
    def addkey(self, key, initdict=None): 
        """add a new key to index, each key is a node
        key := the name for the key to create
        initdict := optionally initialize subkey dictionary
        if key is unique, return ==> True
        if key is not unique, return ==> False 
        """
        if key in self.index:
            self.index[self.whoami(pre='log')].debug( __name__ + ": err, key already exists")
            return False
        else:
            self.index[key]= self.mkNode(initdict)
            return True

    def remkey(self, key):
        """remove a existing key from index
        key := the name of the key to remove
        if the key exist ==> True
        if the key doesn't exist ==> False
        """
        #?: how should this be handled, (1) unlink, or (2) cascading delete?
        if key in self.index:
            #preop hook
            del self.index[key]
            #postop hook
            return True
        else:
            return False

    def getkey(self, key=None):
        """retrieve key specified
        if the key exists ==> return key/vals in index[key] 
        else ==> return index
        """
        if key in self.index:
            #all's well, drop to return
            pass
        elif self.addkey(key):
            #implicitly add new key
            #TODO: add hook, implicitly adding key
            pass
        else:
            #undefined failure
            self.index[self.whoami(pre='log')].debug("err: key creation by inference failed")

        return self.index[key]
        
    def mkNode(self,initdict=None):
        """
        emulate a defaultdict from the collections class
        """
        #?: is this the proper way to initialize a new Node(), perhaps __new__ would be more appropriate?
        #TODO: add hook, creating new Node()
        return Node(initdict) 

    ## handle logging
    def getLog( self, name=None ):
        """
        """
        #TODO: move these configs to either defaults or some auto-updating key
        log_name = self.whoami( pre='log',post=name )
        log_format = logging.Formatter('%(asctime)s :: %(levelname)-8s: %(name)-15s :: %(message)s')
        log_level  = logging.CRITICAL #nb: default is 'WARNING', alt. DEBUG,INFO,
        log_output = {'console' : logging.StreamHandler( sys.stdout ),
                      'file': logging.FileHandler( log_name + '.log' ) }
        #create this log is it doesn't already exist
        if log_name not in self.index:
            self.index[log_name] = logging.getLogger( log_name )
            # set config
            log_output['console'].setFormatter( log_format )
            self.index[log_name].setLevel( log_level )
            self.index[log_name].addHandler( log_output['console'] )
            # declare existence
            self.index[log_name].debug("log is activated.")

        return self.index[log_name]

    def default( self, initdict ):
        """
        """
        pass

    def reflex(self, target=None):
        """update reflection data
        """
        if not target: target = self
        #TODO: add logic to update Node settings from self.index 
        #NB: by embedding log in self.index we _kinda_ make access uniform
        self.index['__id'] = id(target)
        self.index['__type'] = type(target)
        #TODO: expand w/ options and ensure available to higher classes
        #NB: potential keys as seen in keyset above, idealy these could be added programmatically using hooks

    def whoami(self, pre=None ,post=None):
        """local reference
        """
        nametag = []
        if pre: nametag.append( pre )
        if self.index['__id']: nametag.append( self.index['__id'] )
        if self.index['__type']: nametag.append( self.index['__type'] )
        if post: nametag.append( post )

        for i in range( len(nametag) ):
            nametag[i] = str( nametag[i] )

        nametag = ".".join(nametag)
        return nametag

    def __init__(self, initdict=None):
        """class constructor
        initdict := key/value pairs copied to index
        """
        self.index = {}
        self.reflex()
        self.getLog()

        #if exists, add/overwrite keys
        if initdict:
            self.index[self.whoami(pre="log")].debug("init config found, seeding node-index")
            #?: should this use self.addkey() instead? 
            self.index.update( initdict )
        else:
            self.default()
        
    def __del__(self):
        """erase and remove this Node and the index it contains
        """
        #TODO: unify/fold class and configuration for logging into self.index 
        self.getLog().debug("removing node")

        #interesting notion, adding getLog function to self.index[log]
        #self.index[log].getLog(__name__).debug("removing node") #interesting notion
	self.index.clear()
        del self.index

    def __call__(self):
        """return index
        """
        return self.index

    def __getitem__(self,key):
        """
        """
        #?: overload to detect and dereference DataNode symlinks
        return self.getkey(key)
        
        
class DataNode():    
    """ data level
    attribute = (domain=NULL, simple_relation[n]) #domain is optional, default:NULL
    simple_relation = (something, relationship, something)     
    def __init__(self, attributes ):
    """
    pass

class RelationNode:
    """
    def __init__(self):
		#load relations
		#load object
		#load subjects
		#pass
    #def generate(self,subject,relation,object):
    """
    pass

class AttributeNode:
    """
    def __init__(self, domain, relationship):
    """
    pass

class InfoNode:
    """ information level
    info = (notion[n], attribute[n])
    notion[n] = (symbol_num, symbol_name) #symbol_name is optional, default: NULL
    pattern[n] = #? not sure what this is
    """
    pass

class DomainNode:
    """ domain-dependent logical object model
    context = ontological_realm #defines the permissable operations and relations
    constraints = conditions #defines specific applicable conditional limitations and exceptions 
    def __init__(self, context=NULL, constraints=NULL):
    """
    pass

class KnowledgeNode:
    """ knowledge node
    stereotype = #? pattern analysis on notion
    concept = #? intra-domain information aggregate and linking
    """
    pass

class WisdomNode:
    """ inter-domain logical object model
    this is the part that is truly dificult to model
    presently i don't have much of an idea how to acheive this
    """
    pass

#-function defs
pass
#-main--begin----------
pass  
#-main--end------------
